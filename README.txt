--------------------------------------------------------
FhDb (uses Entity Framework to create a Sqlite database)
--------------------------------------------------------
To create a database using Entity Framework Code First:
These articles are pretty thorough:
https://docs.microsoft.com/en-us/ef/ef6/modeling/code-first/workflows/new-database
https://docs.microsoft.com/en-us/ef/ef6/modeling/code-first/migrations/)
however, there are some extra steps when using Sqlite.

In short:
1. Create a C# console project (.Net Framework).
2. Using Nuget or Package Manager Console, add System.Data.Sqlite.EF6 and System.Data.Sqlite.EF6.Migrations
3. Create model classes 
4. Create a context class
5. Run migrations to create the database
	a. In Package Manager Console, run the command Enable-Migrations. This will create a Migrations folder and a configuration.cs class
	b. In the Configuration class, update the constructor so it looks like this:
	        public Configuration()
			{
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("System.Data.SQLite", new SQLiteMigrationSqlGenerator());
			}
	c. Add a connection string to the app.config file to specify the location of the database file. If you give the connection string the
		same name as your database, EF will use the connection string automatically, e.g.
		<connectionStrings>
			<add name="name-of-database" connectionString="Data Source=C:\ProgramData\mvvmsamples\name-of-database.db" providerName="System.Data.SQLite" />
		</connectionStrings>
	d. Make sure this provider is included in the app.config:
		<provider invariantName="System.Data.SQLite" type="System.Data.SQLite.EF6.SQLiteProviderServices, System.Data.SQLite.EF6" />
	e. And this is the System.Data section of the app.config:
		<DbProviderFactories>
			<remove invariant="System.Data.SQLite" />
			<add name="SQLite Data Provider" invariant="System.Data.SQLite" description=".NET Framework Data Provider for SQLite" type="System.Data.SQLite.SQLiteFactory, System.Data.SQLite" />
		</DbProviderFactories>
	f. In Package Manager Console, use the command Add-Migration InitialCommit to add a migration file called InitialCommit
	g. In Package Manager Console, use the command Update-Database to create the database
6. Write code to read / write from database as needed. 


------------------------------------------------
MvvmNavigation (uses Mvvm Light Messenger class)
------------------------------------------------
This sample was intended to show how to navigate between the main window and various user controls
(e.g. in a tab control, menu control, etc).  There is a sidebar with buttons to change the content of the main window.
It's simple enough to bind the sidebar buttons in the MainWindow with commands in the MainViewModel, so that MainViewModel
re-binds the ContentControl/CurrentPageViewModel to different view models, depending on which sidebar button was clicked.

In the .xaml file, define a content area, and a button that is bound to a command
        <!-- Content Area -->
        <ContentControl Content="{Binding CurrentPageViewModel}" />

        <!-- Button which will be wired to set the Content Area -->
		<Button Content="Load People" Width="150" Height="25" Command="{Binding LoadPersonsCommand}" />

In the view model class, define the command as a property 
       public ICommand LoadPersonsCommand { get; private set; }

	   // Wire a handler method to the command
       LoadPersonsCommand = new RelayCommand(LoadPersons);
		
	   // Provide a method which updates the view model bound to the ContentControl
		public void LoadPersons()
        {
            ChangeViewModel(new PersonListViewModel());
            return;
        }


What's more complex, though, is when you want a button in the content window to change the binding of the 
ContentControl/CurrentPageViewModel. The usual way that view models communicate with one another is via the 
Mediator design pattern, but implementing it ourselves is beyond the scope of simple navigation.

The Mvvm Light library includes a very easy-to-use class called Messenger, which is a messaging bus that enables viewmodels 
to communicate with one another.  Any view model that needs to receive messages subscribes/registers for them like this:
          
		  Messenger.Default.Register<NotificationMessage>(this, NotifyMe);

then provides a method to handle incoming messages:
       
	   public void NotifyMe(NotificationMessage notificationMessage)
        {
            string mymessage = notificationMessage.Notification;
			// do something with mymessage
		}

If a view model wants to send a message, it's just a single line of code:
          
		  Messenger.Default.Send<NotificationMessage>(new NotificationMessage("message to send"));