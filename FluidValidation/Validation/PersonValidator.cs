﻿using FluentValidation;
using FluidValidation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluidValidation.Validation
{
    public class PersonValidator : FluentValidator<Person>
    {
        public PersonValidator()
        {
            RuleFor(person => person.PersonId).NotNull();
            RuleFor(person => person.FirstName).NotEmpty().WithMessage("Please enter the first name");
            RuleFor(person => person.LastName).NotEmpty();
            RuleFor(person => person.Street).NotEmpty();
            RuleFor(person => person.City).NotEmpty();
            RuleFor(person => person.State).NotEmpty();
            RuleFor(person => person.Zip).NotEmpty().Matches(@"^\d{5}$"); // 5 digit number
            RuleFor(person => person.PhoneNbr).NotEmpty();
            RuleFor(person => person.Website).NotEmpty();
            RuleFor(person => person.Email).NotEmpty().EmailAddress();
        }
    }
}
