﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FluidValidation.ViewModel
{
    public abstract class ValidatableViewModelBase : ViewModelBase
    {
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public abstract void ValidateAllProperties();

        protected virtual bool SetAndValidateAll<T>(
            ref T storage,
            T value,
            [CallerMemberName] string propertyName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            var result = this.Set(ref storage, value, propertyName);

            if (result)
            {
                this.ValidateAllProperties();
            }

            return result;
        }

        protected virtual void OnErrorsChanged(DataErrorsChangedEventArgs e)
        {
            var handler = this.ErrorsChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
