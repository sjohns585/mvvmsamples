using DTOService.DataServices;
using DTOService.Models;
using FluidValidation.Model;
using FluidValidation.Validation;
using FluidValidation.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace FluentValidation.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private Person _person;
        private string result;
        private IList<string> _allerrors;
        public IList<string> AllErrors
        {
            get { return _allerrors; }
            set { this.Set(ref _allerrors, value); }
        }
        public Person CurrentPerson { get; set; }
 /*       {
            get { return _person; }
            set
            { Set(ref _person, value); }
        }*/
        public string Result
        {
            get { return result; }
            set { Set(ref result, value); }
        }
        private void ClearResult()
        {
            this.Result = string.Empty;
        }
        private readonly RelayCommand _updatePersonCommand;
        public ICommand UpdatePersonCommand => _updatePersonCommand;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
          //  _updatePersonCommand = new RelayCommand(OnUpdatePerson);
           // _updatePersonCommand = new RelayCommand(OnUpdatePerson, CanUpdatePerson);

            PeopleService ps = new PeopleService();
            CurrentPerson = GetPerson();
            CurrentPerson.ValidateAllProperties();
            UpdateAllErrors();
        }
        private Person GetPerson()
        {
            Random rnd = new Random();
            int current = rnd.Next(1, 10);
            PeopleService ps = new PeopleService();
            List<Mvvm_Person> plist = ps.GetList();
            return MapPerson(plist[current - 1]);
        }
        private void OnUpdatePerson(object commandParameter)
        {
            if (CurrentPerson.HasErrors == true)
            {
                MessageBox.Show("Please fix errors first!");
                return;
            }

            MessageBox.Show("Are you sure?");
            PeopleService ps = new PeopleService();
            string result = ps.UpdatePerson(MapPerson(CurrentPerson));
            if (string.IsNullOrEmpty(result))
            {
                MessageBox.Show(string.Format("PersonID {0} ({1} {2}) updated successfully", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName));
                return;
            }
            else
            {
                MessageBox.Show(string.Format("Error updating personID {0} ({1} {2}): {3}", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName, result));
                return;
            }

        }
        private bool CanUpdatePerson(object commandParameter)
        {
            // Can always update
            return true;
        }
 
        private void UpdateAllErrors()
        {
            AllErrors = CurrentPerson.GetAllErrors();
        }
        #region mapping
        public Person MapPerson(Mvvm_Person mp)
        {
            Person p = new Person(new PersonValidator());
            p.PersonId = mp.PersonId;
            p.FirstName = mp.FirstName;
            p.LastName = mp.LastName;
            p.Street = mp.Street;
            p.City = mp.City;
            p.State = mp.State;
            p.Zip = mp.Zip;
            p.Website = mp.Website;
            p.PhoneNbr = mp.PhoneNbr;
            p.DOB = mp.DOB;
            return p;

        }
        public Mvvm_Person MapPerson(Person p)
        {
            Mvvm_Person mp = new Mvvm_Person();
            mp.PersonId = (int)p.PersonId;
            mp.FirstName = p.FirstName;
            mp.LastName = p.LastName;
            mp.Street = p.Street;
            mp.City = p.City;
            mp.State = p.State;
            mp.Zip = p.Zip;
            mp.Website = p.Website;
            mp.PhoneNbr = p.PhoneNbr;
            mp.DOB = p.DOB;
            return mp;

        }
        #endregion

    }
}