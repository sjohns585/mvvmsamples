﻿using FluidValidation.ViewModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluidValidation.Model
{
    public class Person : ValidatableViewModelBase, INotifyDataErrorInfo
    {
        private readonly IValidator<Person> _validator;
        private long _personid;
        private string _lastname;
        private string _firstname;
        private string _street;
        private string _city;
        private string _state;
        private string _zip;
        private string _phonenbr;
        private string _email;
        private string _website;
        private DateTime _dateofbirth;
        private string result;

        public Person(IValidator<Person> validator)
        {
            _validator = validator;
        }
        public long PersonId
        {
            get{ return _personid; }
            set
            { 
               if (SetAndValidateAll(ref _personid, value))
                    ClearResult();
            }
        }
        public string FirstName
        {
            get{ return _firstname; }
            set
            {
                if (SetAndValidateAll(ref _firstname, value))
                    ClearResult();
            }
        }
        public string LastName
        {
            get { return _lastname; }
            set
            {
                if (SetAndValidateAll(ref _lastname, value))
                    ClearResult();
            }
        }
        public string Street
        {
            get { return _street; }
            set
            {
                if (SetAndValidateAll(ref _street, value))
                    ClearResult();
            }
        }
        public string City
        {
            get { return _city; }
            set
            {
                if (SetAndValidateAll(ref _city, value))
                    ClearResult();
            }
        }
        public string State
        {
            get { return _state; }
            set
            {
                if (this.SetAndValidateAll(ref _state, value))
                    ClearResult();
            }
        }
        public string Zip
        {
            get { return _zip; }
            set
            {
                if (this.SetAndValidateAll(ref _zip, value))
                    ClearResult();
            }
        }
        public string PhoneNbr
        {
            get { return _phonenbr; }
            set
            {
                if (this.SetAndValidateAll(ref _phonenbr, value))
                    ClearResult();
            }
        }
        public DateTime DOB
        {
            get { return _dateofbirth; }
            set
            {
                if (this.SetAndValidateAll(ref _dateofbirth, value))
                    ClearResult();
            }
        }
        public string Website
        {
            get { return _website; }
            set
            {
                if (this.SetAndValidateAll(ref _website, value))
                    ClearResult();
            }
        }
        public string Email
        {
            get { return _email; }
            set
            {
                if (this.SetAndValidateAll(ref _email, value))
                    ClearResult();
            }
        }
        public string Result
        {
            get { return result; }
            set { Set(ref result, value); }
        }

        public bool HasErrors
        {
            get { return _validator.HasErrors; }
        }
        private void ClearResult()
        {
            this.Result = string.Empty;
        }

        public override void ValidateAllProperties()
        {
            _validator.Validate(this);
        }
        public IList<string> GetAllErrors()
        {
            return _validator.GetAllErrors();
        }
        public IEnumerable GetErrors(string propertyName)
        {
            return _validator.GetErrors(propertyName);
        }
    }
}
