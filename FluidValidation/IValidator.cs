﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluidValidation
{
    public interface IValidator<in T> : INotifyDataErrorInfo
    {
        IDictionary<string, string> Validate(T instance);

        IList<string> GetAllErrors();
    }
}
