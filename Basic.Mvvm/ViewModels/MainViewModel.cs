﻿using Basic.Mvvm.Helpers;
using DTOService.DataServices;
using DTOService.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Basic.Mvvm.ViewModels
{
    // from https://intellitect.com/getting-started-model-view-viewmodel-mvvm-pattern-using-windows-presentation-framework-wpf/
    public class MainViewModel : ViewModelBase
    {

        // If we weren't inheriting from ViewModelBase, we'd need a PropertyChangedEventHandler, and we'd need to call it
        // every time a property changed 
        //        public event PropertyChangedEventHandler PropertyChanged;
        //        public void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName);

        private Mvvm_Person _person;
        public Mvvm_Person CurrentPerson
        {
            get => _person;
            set => SetProperty(ref _person, value);
        }
        private ObservableCollection<Mvvm_Person> _personlist;
        public ObservableCollection<Mvvm_Person> PersonList
        {
            get
            {
                return _personlist;
            }
        }

        private readonly RelayCommand _updatePersonCommand;
        public ICommand UpdatePersonCommand => _updatePersonCommand;
        private readonly RelayCommand _getnewPersonCommand;
        public ICommand GetNewPersonCommand => _getnewPersonCommand;
        public MainViewModel()
        {
            _updatePersonCommand = new RelayCommand(OnUpdatePerson, CanUpdatePerson);
            _getnewPersonCommand = new RelayCommand(OnGetNewPerson, CanGetNewPerson);
            if (_personlist == null)
                _personlist = new ObservableCollection<Mvvm_Person>();

            if (_personlist.Count == 0)
            {
                PeopleService ps = new PeopleService();
                List<Mvvm_Person> plist = ps.GetList();
                foreach (Mvvm_Person p in plist)
                    _personlist.Add(p);
                CurrentPerson = GetPerson();
            }
            CurrentPerson = GetPerson();
        }
        private void OnUpdatePerson(object commandParameter)
        {
            SavePerson(CurrentPerson);
            _updatePersonCommand.InvokeCanExecuteChanged();
        }
        private void OnGetNewPerson(object commandParameter)
        {
            CurrentPerson = GetPerson();
            _getnewPersonCommand.InvokeCanExecuteChanged();
        }
        private Mvvm_Person GetPerson()
        {
            Random rnd = new Random();
            int current = rnd.Next(1, 10);
            PeopleService ps = new PeopleService();
            return ps.GetById(current - 1);
        }
        private bool SavePerson(Mvvm_Person p)
        {
            PeopleService ps = new PeopleService();
            string result = ps.UpdatePerson(p);
            if (string.IsNullOrEmpty(result))
            {
                MessageBox.Show(string.Format("PersonID {0} ({1} {2}) updated successfully", p.PersonId, p.FirstName, p.LastName));
                return true;
            }
            else
            {
                MessageBox.Show(string.Format("Error updating personID {0} ({1} {2}): {3}", p.PersonId, p.FirstName, p.LastName, result));
                return false;
            }
        }

        private bool CanUpdatePerson(object commandParameter)
        {
            // Can always update, except for Mike Mitchell
            //  return CurrentPerson.FirstName != "Mike";
            return true;
        }
        private bool CanGetNewPerson(object commandParameter)
        {
            // Once we happen upon final person, we are done getting new people
            //  return CurrentPerson.FirstName != "Final";
            return true;
        }
    }
}
