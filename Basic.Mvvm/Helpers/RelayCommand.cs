﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Basic.Mvvm.Helpers
{
    // Use this interface to assign command objects to properties in the view model. 
    // Unfortunately WPF does not come with a default ICommand implementation suitable for use in a view model, 
    // but this is a simple way of implementing it

    // In this very simple implementation, an Action delegate is invoked when the command is executed.

    // (Often named RelayCommand) Both are an implementation of ICommand which works by accepting a delegate and using 
    // that to provide the ICommand implementation. As such, both classes have the same intent, and work in basically the same manner.
    // As for differences - there may be some subtle diffferences, depending on which framework you're using. For example, 
    // Prism's DelegateCommand<T> also has a concept of IActiveAware, which is used for building composite commands.
    public class RelayCommand : ICommand
    {
        private readonly Action<object> _executeAction;
        private readonly Func<object, bool> _canExecuteAction;
        public RelayCommand(Action<object> executeAction)
        {
            _executeAction = executeAction;
        }

        public RelayCommand(Action<object> executeAction, Func<object, bool> canExecuteAction)
        {
            _executeAction = executeAction;
            _canExecuteAction = canExecuteAction;
        }

        public void Execute(object parameter) => _executeAction(parameter);

        // Without this ( below), the command button would always be enabled.
        public bool CanExecute(object parameter) => _canExecuteAction?.Invoke(parameter) ?? true;

        public event EventHandler CanExecuteChanged;

        public void InvokeCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);
    }

}