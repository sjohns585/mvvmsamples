﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    [Table("Fumehood")]

    public class Fumehood
    {
        [Key]
        public int FumehoodId { get; set; }
        public string Building { get; set; }
        public int FumehoodCustomerId { get; set; }
        public Customer ParentCustomer { get; set; }
        public List<Report> RelatedReports { get; set; }

    }
}
