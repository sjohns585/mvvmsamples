﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    [Table("Restaurant")]
    public class Restaurant
    {
        [Key]
        public string RestaurantId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Price { get; set; }
        public bool IsSelected { get; set; }
    //    public List<MenuItem> RelatedMenuItems { get; set; }
    }
}
