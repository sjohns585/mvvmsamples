﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    [Table("Report")]
    public class Report
    {
        [Key]
        public int ReportId { get; set; }
        public int ReportFumehoodId { get; set; }
        public Fumehood ParentFumehood { get; set; }

    }
}
