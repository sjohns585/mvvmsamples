﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Data.SQLite.EF6.Migrations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    public class TestContext : DbContext, IDbContext
    {
        // constructor, which will also call DbContext's (base) constructor with the default connection string
        public TestContext() : base("TestContext")
        {
            // Problematic! Not using at this time
            //     Database.SetInitializer<TestContext>(new CreateDatabaseIfNotExists<TestContext>());
       //     Database.SetInitializer<TestContext>(new DropCreateDatabaseIfModelChanges<TestContext>());
        }

        // Use the Fluent Api to configure foreign key constraints for a relationship
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<TestContext>(null);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

            // Set up foreign key between Customer and Site tables
            modelBuilder.Entity<Customer>()
                        .HasMany(s => s.sites)
                        .WithRequired(s => s.Customer)
                        .HasForeignKey(s => s.CustomerId);
        }
        public DbSet<Customer> customers { get; set; }
        public DbSet<Site> sites { get; set; }
        public DbSet<Person> persons { get; set; }
        public DbSet<Restaurant> restaurants { get; set; }
    }
    public class FhDbInitializer : CreateDatabaseIfNotExists<TestContext>
    {
       protected override void Seed(TestContext context)
        {
            try
            {
                DbUtil util = new DbUtil();
                util.SeedDatabase();

                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                //                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Unexpected exception:" + ex.Message));
            }

            base.Seed(context);
        }
    }
}