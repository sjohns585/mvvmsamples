﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    [Table("MenuItem")]
    public class MenuItem
    {
        [Key, Column(Order = 0), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string MenuItemId { get; set; }
        [Key, Column(Order = 1)]
        public string RestaurantId { get; set; }
        public string MenuItemName { get; set; }
        public string MenuItemDescription { get; set; }
        public Restaurant ParentRestaurant { get; set; }
    }
}
