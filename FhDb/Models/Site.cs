﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    [Table("Site")]
    public class Site
    {
        [Key]
        public int Id { get; set; }
        public string SiteName { get; set; }
        public string SiteShortName { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
