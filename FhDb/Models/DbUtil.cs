﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FhDb.Models
{
    public class DbUtil
    {
        public void SeedDatabase()
        {
            TestContext context = new TestContext();
            // No need if there is already data in the table
            if (context.persons.Any()) return;

            try
            {
                context.persons.Add(new Person { PersonId = 1, FirstName = "Mike", LastName = "Mitchell", Street = "111 Main Street", State = "IN", City = "Monrovia", Zip = "46111", PhoneNbr = "3175551211", DOB = DateTime.Parse("01-10-1995") });
                context.persons.Add(new Person { PersonId = 2, FirstName = "Ned", LastName = "Nelson", Street = "222 Main Street", State = "IN", City = "Noblesville", Zip = "46112", PhoneNbr = "(3174978644", DOB = DateTime.Parse("02-10-2002") });
                context.persons.Add(new Person { PersonId = 3, FirstName = "Oscar", LastName = "Orwell", Street = "333 Main Street", State = "IN", City = "Osceola", Zip = "46113", PhoneNbr = "3179876454", DOB = DateTime.Parse("03-10-1970") });
                context.persons.Add(new Person { PersonId = 4, FirstName = "Peter", LastName = "Piper", Street = "444 Main Street", State = "IN", City = "Porter", Zip = "46114", PhoneNbr = "3175554321", DOB = DateTime.Parse("04-10-1986") });
                context.persons.Add(new Person { PersonId = 5, FirstName = "Final", LastName = "Quigley", Street = "555 Main Street", State = "IN", City = "Qville", Zip = "46115", PhoneNbr = "3177654321", DOB = DateTime.Parse("05-10-1991") });
                context.persons.Add(new Person { PersonId = 6, FirstName = "Randy", LastName = "Ross", Street = "666 Main Street", State = "IN", City = "Richmond", Zip = "46116", PhoneNbr = "3178879121", DOB = DateTime.Parse("06-10-1998") });
                context.persons.Add(new Person { PersonId = 7, FirstName = "Stuart", LastName = "Strong", Street = "777 Main Street", State = "IN", City = "Santa Claus", Zip = "46117", PhoneNbr = "(3175946711", DOB = DateTime.Parse("07-10-1982") });
                context.persons.Add(new Person { PersonId = 8, FirstName = "Terry", LastName = "Tuttle", Street = "888 Main Street", State = "IN", City = "Trafalgar", Zip = "46118", PhoneNbr = "3178786322", DOB = DateTime.Parse("08-10-1977") });
                context.persons.Add(new Person { PersonId = 9, FirstName = "Ulysses", LastName = "Unvers", Street = "999 Main Street", State = "IN", City = "Union City", Zip = "46119", PhoneNbr = "3173336473", DOB = DateTime.Parse("09-10-1989") });
                context.persons.Add(new Person { PersonId = 10, FirstName = "Vernon", LastName = "Vandersteen", Street = "101010 Main Street", State = "IN", City = "Valparaiso", Zip = "46110", PhoneNbr = "3173456217", DOB = DateTime.Parse("10-10-1993") });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                //                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Unexpected exception:" + ex.Message));
            }
        }

    }
}
