namespace FhDb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerName = c.String(maxLength: 2147483647),
                        ShortName = c.String(maxLength: 2147483647),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Site",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SiteName = c.String(maxLength: 2147483647),
                        SiteShortName = c.String(maxLength: 2147483647),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 2147483647),
                        LastName = c.String(maxLength: 2147483647),
                        Street = c.String(maxLength: 2147483647),
                        City = c.String(maxLength: 2147483647),
                        State = c.String(maxLength: 2147483647),
                        Zip = c.String(maxLength: 2147483647),
                        PhoneNbr = c.String(maxLength: 2147483647),
                        DOB = c.DateTime(nullable: false),
                        Website = c.String(maxLength: 2147483647),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.Restaurant",
                c => new
                    {
                        RestaurantId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(maxLength: 2147483647),
                        Type = c.String(maxLength: 2147483647),
                        City = c.String(maxLength: 2147483647),
                        State = c.String(maxLength: 2147483647),
                        Price = c.String(maxLength: 2147483647),
                        IsSelected = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RestaurantId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Site", "CustomerId", "dbo.Customer");
            DropIndex("dbo.Site", new[] { "CustomerId" });
            DropTable("dbo.Restaurant");
            DropTable("dbo.Person");
            DropTable("dbo.Site");
            DropTable("dbo.Customer");
        }
    }
}
