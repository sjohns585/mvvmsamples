﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOService.Models
{
    public class Mvvm_Person : ViewModelBase, IDataErrorInfo
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNbr { get; set; }
        public DateTime DOB { get; set; }
        public string Website { get; set; }

        public string Error => throw new NotImplementedException();
        public string this[string propertyName]
        {
            get { return GetValidationError(propertyName); }
        }
        public string GetValidationError(string propname)
        {
            string errorMsg = string.Empty;

            switch (propname)
            {
                case "FirstName":
                    if (String.IsNullOrEmpty(FirstName))
                        errorMsg = "First Name is required";
                    break;
                case "LastName":
                    if (String.IsNullOrEmpty(LastName))
                        errorMsg = "Last Name is required";
                    break;
                case "Street":
                    if (String.IsNullOrEmpty(Street))
                        errorMsg = "Street is required";
                    break;
                case "City":
                    if (String.IsNullOrEmpty(City))
                        errorMsg = "City is required";
                    break;
                case "State":
                    if (String.IsNullOrEmpty(State))
                        errorMsg = "State is required";
                    break;
                case "Zip":
                    if (String.IsNullOrEmpty(Zip))
                        errorMsg = "Zip code is required";
                    else if (IsDigitsOnly(Zip) == false)
                        errorMsg = "Zip code must only contain numbers";
                    break;
                case "Phone":
                    if (String.IsNullOrEmpty(PhoneNbr))
                        errorMsg = "Phone Number is required";
                    else if (IsDigitsOnly(PhoneNbr) == false)
                        errorMsg = "Phone number must only contain numbers";
                    break;

            }
            return errorMsg;
        }
        protected bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                {
                    if (c != ')' && c != '(' && c != '=' && c != '/')
                        return false;
                }
            }

            return true;
        }
    }
}
