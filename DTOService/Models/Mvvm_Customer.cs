﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOService.Models
{
    public class Mvvm_Customer : ViewModelBase, IDataErrorInfo
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string ShortName { get; set; }
        #region IDataErrorInfo 
        public string this[string columnName] => throw new NotImplementedException();
        public string Error => throw new NotImplementedException();
        #endregion

    }
}
