﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOService.Models
{
    public class Mvvm_Restaurant : ViewModelBase, IDataErrorInfo
    {
        public string RestaurantId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Price { get; set; }
        public bool IsSelected { get; set; }
        #region IDataErrorInfo 
        public string this[string columnName] => throw new NotImplementedException();
        public string Error => throw new NotImplementedException();
        #endregion

    }
}
