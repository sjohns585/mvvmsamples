﻿using DTOService.Models;
using FhDb.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOService.DataServices
{
    public class CustomerService
    {
        TestContext context = new TestContext();
        public List<Mvvm_Customer> GetList()
        {
            SeedDatabase();
            List<Mvvm_Customer> custlist = new List<Mvvm_Customer>();
            foreach (Customer c in context.customers.ToList())
                custlist.Add(MapCustomer(c));
            return custlist;
        }
        public Mvvm_Customer GetById(int Id)
        {
            return MapCustomer (context.customers.FirstOrDefault(p => p.Id == Id));
        }
        public Mvvm_Customer MapCustomer(Customer c)
        {
            Mvvm_Customer mc = new Mvvm_Customer();
            mc.Id = c.Id;
            mc.CustomerName = c.CustomerName;
            mc.ShortName = c.ShortName;
            return mc;

        }
        protected void SeedDatabase()
        {
            // No need if there is already data in the table
            if (context.customers.Any()) return;

            try
            {
                context.customers.Add(new Customer() { Id = 1, CustomerName = "REI - 86th Street", ShortName = "REI" });
                context.customers.Add(new Customer() { Id = 2, CustomerName = "Panera - 82th Street", ShortName = "PAN82" });
                context.customers.Add(new Customer() { Id = 3, CustomerName = "Speedway - Allisonville Road", ShortName = "SPDAL" });
                context.customers.Add(new Customer() { Id = 4, CustomerName = "DowElanco - 96th Street", ShortName = "DOW96" });
                context.sites.Add(new Site() {Id = 1, CustomerId = 2, SiteName = "Main" });
                context.sites.Add(new Site() { Id = 2758942, CustomerId = 2, SiteName = "Main" });
                context.sites.Add(new Site() { Id = 3579432, CustomerId = 2, SiteName = "Service" });
                context.sites.Add(new Site() { Id = 4579432, CustomerId = 4, SiteName = "North Lab" });
                context.sites.Add(new Site() { Id = 4254432, CustomerId = 4, SiteName = "North Lab" });
                context.sites.Add(new Site() { Id = 4323432, CustomerId = 4, SiteName = "North Lab" });
                context.sites.Add(new Site() { Id = 1234432, CustomerId = 4, SiteName = "North Lab" });
                context.sites.Add(new Site() { Id = 7094325, CustomerId = 4, SiteName = "South Lab" });
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Unexpected exception:" + ex.Message));
            }
        }

    }

}
