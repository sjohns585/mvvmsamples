﻿using DTOService.Models;
using FhDb.Models;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOService.DataServices
{
    public class RestaurantService
    {
        private TestContext _context;
        public RestaurantService()
        {
            _context = new TestContext();
        }
        public Mvvm_Restaurant MapRestaurant(Restaurant r)
        {
            Mvvm_Restaurant mr = new Mvvm_Restaurant();
            mr.RestaurantId = r.RestaurantId;
            mr.State = r.State;
            mr.City = r.City;
            mr.Name = r.Name;
            mr.Price = r.Price;
            mr.Type = r.Type;
            return mr;

        }
        public List<Mvvm_Restaurant> GetList()
        {
            SeedDatabase();
            List<Mvvm_Restaurant> restlist = new List<Mvvm_Restaurant>();
            foreach (Restaurant r in _context.restaurants.ToList())
                restlist.Add(MapRestaurant(r));

            return restlist;
        }
        public Mvvm_Restaurant GetById(string Id)
        {
            return MapRestaurant(_context.restaurants.FirstOrDefault(p => p.RestaurantId == Id));
        }
        protected void SeedDatabase()
        {
            // No need if there is already data in the table
            if (_context.restaurants.Any()) return;

            try
            {
                _context.restaurants.Add(new Restaurant { RestaurantId = "BRAVO", Name = "BRAVO Cucina Italiana", Type = "Italian", Price = "$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "MELT", Name = "The Melting Pot", Type = "Fondue", Price = "$$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "BW", Name = "Buffalo Wild Wings", Type = "Sports Bar", Price = "$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "FLEM", Name = "Fleming's Steakhouse", Type = "Steak", Price = "$$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "MERLO", Name = "Eddie Merlot's Prime Aged Beef & Seafood", Type = "Steak", Price = "$$$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "BENI", Name = "Benihana", Type = "Japanese", Price = "$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "4DAY", Name = "Four Day Ray Brewing", Type = "Americane", Price = "$$", State = "IN", City = "Indianapolis" });
                _context.restaurants.Add(new Restaurant { RestaurantId = "VANGRD", Name = "The Vanguard", Type = "Farm-to-table", Price = "$4$", State = "IN", City = "Indianapolis" });
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                Messenger.Default.Send<NotificationMessage>(new NotificationMessage("Unexpected exception:" + ex.Message));
            }
        }

    }

}
