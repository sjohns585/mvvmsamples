using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Messaging;
using MvvmTabs.Helpers;
using System.Collections.ObjectModel;
using System.Security.Principal;

namespace MvvmTabs.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        private ObservableCollection<ITabViewModel> _tabitemviewmodels;
        public ObservableCollection<ITabViewModel> TabItemViewModels
        {
            get { return _tabitemviewmodels ?? (_tabitemviewmodels = new ObservableCollection<ITabViewModel>()); }
            set { Set(ref _tabitemviewmodels, value); }
        }
        // User authentication properties
        private WindowsIdentity _winIdentity;
        private WindowsPrincipal _winPrincipal;
        private ViewModelBase _selectedtab;
        private UserEnvironment _userenv;
        public UserEnvironment UserEnvironmentInfo
        {
            get { return _userenv; }
            set { Set(ref _userenv, value); }
        }
        public ViewModelBase SelectedTab
        {
            get { return _selectedtab; }
            set { Set(ref _selectedtab, value); }
        }

        public MainViewModel()
        {
            _winPrincipal = GetCurrentUser();
            UserEnvironmentInfo.WelcomeLabel = string.Format("Welcome, {0} on machine {1}!", UserEnvironmentInfo.UserName, UserEnvironmentInfo.MachineName);
            CreateTabs();
        }
        private WindowsPrincipal GetCurrentUser()
        {
            _winIdentity = System.Security.Principal.WindowsIdentity.GetCurrent();
            string username = _winIdentity.Name;
            if (UserEnvironmentInfo == null)
                UserEnvironmentInfo = new UserEnvironment();
            UserEnvironmentInfo.MachineName = System.Environment.MachineName;
            UserEnvironmentInfo.UserName = _winIdentity.Name;

            return new System.Security.Principal.WindowsPrincipal(_winIdentity);
        }
        public void ShowStatus(string output)
        {
            UserEnvironmentInfo.AlertMessage = output;
        }
        /// <summary>
        /// Used to bind any incoming status messages, to the MainWindow status bar.
        /// </summary>
        public string StatusBarMessage
        {
            get { return _statusBarMessage; }
            set
            {
                if (value == _statusBarMessage) return;
                _statusBarMessage = value;
                RaisePropertyChanged("StatusBarMessage");
            }
        }
        private string _statusBarMessage;

        private void CreateTabs()
        {
            // Instantiate the view model collection, then add the two view models (as tabs) that are always included (Home and Data)
            TabItemViewModels = new ObservableCollection<ITabViewModel>();
            TabItemViewModels.Add(SimpleIoc.Default.GetInstance<RestaurantListViewModel>());
            TabItemViewModels.Add(SimpleIoc.Default.GetInstance<DataViewModel>());

            // Register methods with the Messenger class to receive notifications from other places in the field tool.
            Messenger.Default.Register<NavigationMessage>(this, (notifymsg) => ReceiveMessage(notifymsg));
        }
        private void ReceiveMessage(NavigationMessage msg)
        {
            switch (msg.TabName)
            {
                case "rest":
                    ShowStatus(string.Format("Restaurant {0} was selected", msg.Message));
                    break;

                case "data":
                    ShowStatus(msg.Message);
                    break;

                default:
                    break;
            }
        }
    }
}