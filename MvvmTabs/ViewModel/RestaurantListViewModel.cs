﻿using DTOService.DataServices;
using DTOService.Models;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using MvvmTabs.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MvvmTabs.ViewModel
{
    public class RestaurantListViewModel : ViewModelBase, ITabViewModel
    {
        public string Name
        {
            get { return "Restaurant List"; }
        }
        public string Title
        {
            get
            {
                return "Restaurant List";
            }
        }

        private ObservableCollection<Mvvm_Restaurant> _restaurantlist;
        public ObservableCollection<Mvvm_Restaurant> RestaurantList
        {
            get
            {
                return _restaurantlist;
            }
            set
            {
                Set(ref _restaurantlist, value);
            }
        }

        private Mvvm_Restaurant _selectedRestaurant;
        public Mvvm_Restaurant SelectedRestaurant
        {
            get { return _selectedRestaurant; }
            set
            {
                Set(ref _selectedRestaurant, value);
            }
        }
        public ICommand SelectRestaurantCommand { get; private set; }

        public RestaurantListViewModel()
        {
            // Wire method to button(s)
            SelectRestaurantCommand = new RelayCommand(SelectRestaurant);

            if (RestaurantList == null)
            {
                RestaurantList = new ObservableCollection<Mvvm_Restaurant>();
            }

            if (RestaurantList.Count == 0)
            {
                RestaurantService rs = new RestaurantService();
                List<Mvvm_Restaurant> plist = rs.GetList();
                foreach (Mvvm_Restaurant p in plist)
                    RestaurantList.Add(p);

                NavigationMessage nm = new NavigationMessage();
                nm.TabName = "rest";
                nm.Message = "Restaurant list loaded";
                Messenger.Default.Send<NavigationMessage>(nm);
            }
        }
        public void SelectRestaurant()
        {
            if (SelectedRestaurant == null)
            {
                MessageBox.Show("Please select a restaurant");
                return;
            }
            Mvvm_Restaurant p = SelectedRestaurant;

            NavigationMessage nm = new NavigationMessage();
            nm.TabName = "rest";
            nm.Message = SelectedRestaurant.Name;
            Messenger.Default.Send<NavigationMessage>(nm);
        }
    }
}
