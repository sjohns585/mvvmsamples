﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmTabs.ViewModel
{
    public interface ITabViewModel
    {
        string Name { get; }
        string Title { get; }
    }
}
