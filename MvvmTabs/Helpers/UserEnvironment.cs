﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MvvmTabs.Helpers
{
    public class UserEnvironment : INotifyPropertyChanged
    {
        private string _alertmessage { get; set; }
        public string AlertMessage
        {
            get { return _alertmessage; }
            set
            {
                if (value != _alertmessage)
                {
                    _alertmessage = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _appdatatext { get; set; }
        public string AppDataText
        {
            get { return _appdatatext; }
            set
            {
                if (value != _appdatatext)
                {
                    _appdatatext = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _appdatafolder { get; set; }
        public string AppDataFolder
        {
            get { return _appdatafolder; }
            set
            {
                if (value != this._appdatafolder)
                {
                    this._appdatafolder = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _districtlist { get; set; }
        public string DistrictList
        {
            get { return _districtlist; }
            set
            {
                if (value != this._districtlist)
                {
                    this._districtlist = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _fieldtoolversion { get; set; }
        public string FieldToolVersion
        {
            get { return _fieldtoolversion; }
            set
            {
                if (value != this._fieldtoolversion)
                {
                    this._fieldtoolversion = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _machinename { get; set; }
        public string MachineName
        {
            get { return _machinename; }
            set
            {
                if (value != this._machinename)
                {
                    this._machinename = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _serverconnection { get; set; }
        public string ServerConnection
        {
            get { return _serverconnection; }
            set
            {
                if (value != this._serverconnection)
                {
                    this._serverconnection = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private long _serverlatency { get; set; }
        public long ServerLatency
        {
            get { return _serverlatency; }
            set
            {
                if (value != this._serverlatency)
                {
                    this._serverlatency = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _username { get; set; }
        public string UserName
        {
            get => _username;
            set
            {
                if (value != _username)
                {
                    _username = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _useremail { get; set; }
        public string UserEmail
        {
            get { return _useremail; }
            set
            {
                if (value != this._useremail)
                {
                    this._useremail = value;
                    NotifyPropertyChanged();
                }
            }
        }
        private string _welcomelabel { get; set; }
        public string WelcomeLabel
        {
            get { return _welcomelabel; }
            set
            {
                if (value != this._welcomelabel)
                {
                    this._welcomelabel = value;
                    NotifyPropertyChanged();
                }
            }
        }

        // Implement INotifyPropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}