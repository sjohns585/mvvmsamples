﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmTabs.Helpers
{
    public class NavigationMessage
    {
        public string TabName { get; set; }
        public string Message { get; set; }
    }
}
