﻿using DTOService.DataServices;
using DTOService.Models;
using MvvmValidation.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MvvmValidation.ViewModels
{
    public class AlternateViewModel : ViewModelBase
    {
        private List<Mvvm_Person> _personlist;
        public List<Mvvm_Person> PersonList
        {
            get
            {
                return _personlist;
            }
        }

        private readonly RelayCommand _updatePersonCommand;
        public ICommand UpdatePersonCommand => _updatePersonCommand;
        private Mvvm_Person _person;
        public Mvvm_Person CurrentPerson
        {
            get => _person;
            set
            {
                if (value != _person)
                {
                    _person = value;
                    base.NotifyPropertyChanged();
                }
            }
        }
        public AlternateViewModel()
        {
            _updatePersonCommand = new RelayCommand(OnUpdatePerson, CanUpdatePerson);

            if (_personlist == null)
                _personlist = new List<Mvvm_Person>();

            if (_personlist.Count == 0)
            {
                PeopleService ps = new PeopleService();
                List<Mvvm_Person> plist = ps.GetList();
                foreach (Mvvm_Person p in plist)
                    _personlist.Add(p);
                CurrentPerson = GetPerson();
            }
        }
        private void OnUpdatePerson(object commandParameter)
        {
            if (IsValid == false)
            {
                 MessageBox.Show("Please fix errors first!");
                 return;
            }

            MessageBox.Show("Are you sure?");
            PeopleService ps = new PeopleService();
            string result = ps.UpdatePerson(CurrentPerson);
            if (string.IsNullOrEmpty(result))
            {
                MessageBox.Show(string.Format("PersonID {0} ({1} {2}) updated successfully", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName));
                return;
            }
            else
            {
                MessageBox.Show(string.Format("Error updating personID {0} ({1} {2}): {3}", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName, result));
                return;
            }

            _updatePersonCommand.InvokeCanExecuteChanged();
        }
        private Mvvm_Person GetPerson()
        {
            Random rnd = new Random();
            int current = rnd.Next(1, 10);
            PeopleService ps = new PeopleService();
            List<Mvvm_Person> plist = ps.GetList();

            return plist[current - 1];
        }

        private bool CanUpdatePerson(object commandParameter)
        {
            // Can always update
            return true;
        }


        private static readonly string[] ValidatedProperties = 
            {
            "FirstName",
            "LastName",
            "Street",
            "City",
            "State",
            "Zip",
            "Phone",
            "DOB",
            "Website"
        };

        public new bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {
                    if (CurrentPerson.GetValidationError(property) != null)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        protected override void Validate(string propertyname)
        {
            throw new NotImplementedException();
        }
    }
}
