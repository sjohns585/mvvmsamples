﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace MvvmValidation.ViewModels
{
    /// <summary>
    /// Implements INotifyPropertyChanged and INotifyDataErrorInfo
    /// </summary>
    /// A discussion of INotifyDataErrorInfo vs IDataErrorInfo is found here:https://stackoverflow.com/questions/17254847/what-is-the-difference-between-validatesonnotifydataerrors-and-validatesondataer
    /// Basically, ValidatesOnNotifyDataErrors and ValidatesOnDataErrors are used when you want a XAML bound control to validate its input based on an 
    /// interface implemented in the ViewModel/Model. For ValidatesOnNotifyDataErrors that interface is INotifyDataErrorInfo. 
    /// For ValidatesOnDataErrors, it is IDataErrorInfo.
    public abstract class ViewModelBase : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// To make sure that child classes implement a validation method
        /// </summary>
        protected abstract void Validate(string propertyname);


        #region Sync
        // This is for synchronous validation
        protected Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

        // Adds the specified error to the errors collection if it is not 
        // already present, inserting it in the first position if isWarning is 
        // false. Raises the ErrorsChanged event if the collection changes. 
        public void AddError(string propertyName, string error, bool isWarning)
        {
            if (!errors.ContainsKey(propertyName))
                errors[propertyName] = new List<string>();

            if (!errors[propertyName].Contains(error))
            {
                if (isWarning) errors[propertyName].Add(error);
                else errors[propertyName].Insert(0, error);
                NotifyErrorsChanged(propertyName);
            }
        }

        // Removes the specified error from the errors collection if it is
        // present. Raises the ErrorsChanged event if the collection changes.
        public void RemoveError(string propertyName, string error)
        {
            if (errors.ContainsKey(propertyName) &&
                errors[propertyName].Contains(error))
            {
                errors[propertyName].Remove(error);
                if (errors[propertyName].Count == 0) errors.Remove(propertyName);
                NotifyErrorsChanged(propertyName);
            }
        }
        #endregion // Sync

        #region Async
        // This is for async validation
        protected ConcurrentDictionary<string, List<ValidationResult>> asyncErrors = new ConcurrentDictionary<string, List<ValidationResult>>();
        #endregion

        #region common
        protected bool SetProperty<T>(ref T field, T newValue, [CallerMemberName]string propertyName = null)
        {
            bool isPropertyChanged = false;
            // If old value = new value, not really changed. Just return false (because there's no need to update / validate anything).
            // For values that really did change, update property value and invoke OnPropertyChanged event
            if (!EqualityComparer<T>.Default.Equals(field, newValue))
            {
                field = newValue;
                Validate(propertyName);
                // Only invoke OnPropertyChanged if not null
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                isPropertyChanged = true;
            }
            return isPropertyChanged;
        }
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                Validate(propertyName);
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        public bool HasErrors
        {
            get
            {
                // We have two error dictionaries, so check both
                return asyncErrors.Count + errors.Count > 0;
            }
        }

        // object is valid
        public bool IsValid => !HasErrors;


        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                return null;

            // Return sync errors
            if (errors.ContainsKey(propertyName))
                return errors[propertyName];

            // Return async errors
            List<ValidationResult> propertyErrors = null;
            asyncErrors.TryGetValue(propertyName, out propertyErrors);
            return propertyErrors;

        }
        protected void NotifyErrorsChanged([CallerMemberName] String propertyName = "")
        {
            if (ErrorsChanged != null)
                ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
        }
        #endregion
    }


}
