﻿using DTOService.DataServices;
using DTOService.Models;
using MvvmValidation.Attributes;
using MvvmValidation.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MvvmValidation.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private ViewModelBase _currentusercontrolviewmodel;
        public ViewModelBase CurrentUserControlViewModel
        {
            get
            {
                return _currentusercontrolviewmodel;
            }
            set
            {
                if (value != _currentusercontrolviewmodel)
                {
                    _currentusercontrolviewmodel = value;
                    base.NotifyPropertyChanged();
                }
            }
        }
        private string uRLName = null;
        /// <summary>
        /// Example of Attribute Validation. See ReachableURL class in MvvmValidation.Attributes folder. Works!
        /// and is great for things that should be validated asynchronously, but the disadvantage is that each individual
        /// property to be validated needs to be a public property of the viewmodel. 
        /// </summary>
        [ReachableURL]
        public string URLName
        {
            get
            {
                return this.uRLName;
            }
            set
            {
                if (value != this.uRLName)
                {
                    this.uRLName = value;
                    base.NotifyPropertyChanged();
                    ValidateURL(uRLName);
                }
            }
        }
        private Mvvm_Person _person;
        public Mvvm_Person CurrentPerson
        {
            get => _person;
            set
            {
                if (value != _person)
                {
                    _person = value;
                    base.NotifyPropertyChanged();
                    Validate("CurrentPerson");
                }
            }
        }
        private ObservableCollection<Mvvm_Person> _personlist;
        public ObservableCollection<Mvvm_Person> PersonList
        {
            get
            {
                return _personlist;
            }
        }

        private readonly RelayCommand _updatePersonCommand;
        public ICommand UpdatePersonCommand => _updatePersonCommand;

        private readonly RelayCommand _getnewPersonCommand;
        public ICommand GetNewPersonCommand => _getnewPersonCommand;

        public MainViewModel()
        {
            _updatePersonCommand = new RelayCommand(OnUpdatePerson, CanUpdatePerson);
            _getnewPersonCommand = new RelayCommand(OnGetNewPerson, CanGetNewPerson);

            if (_personlist == null)
                _personlist = new ObservableCollection<Mvvm_Person>();

            if (_personlist.Count == 0)
            {
                PeopleService ps = new PeopleService();
                List<Mvvm_Person> plist = ps.GetList();
                foreach (Mvvm_Person p in plist)
                    _personlist.Add(p);
                CurrentPerson = GetPerson();
            }
            CurrentUserControlViewModel = new AlternateViewModel();
        }
        private void OnUpdatePerson(object commandParameter)
        {
            Validate("CurrentPerson");
            if (HasErrors == true)
            {
                MessageBox.Show("Please fix errors first!");
                return;
            }

            MessageBox.Show("Are you sure?");
            PeopleService ps = new PeopleService();
            string result = ps.UpdatePerson(CurrentPerson);
            if (string.IsNullOrEmpty(result))
            {
                MessageBox.Show(string.Format("PersonID {0} ({1} {2}) updated successfully", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName));
                return;
            }
            else
            {
                MessageBox.Show(string.Format("Error updating personID {0} ({1} {2}): {3}", CurrentPerson.PersonId, CurrentPerson.FirstName, CurrentPerson.LastName, result));
                return;
            }

            _updatePersonCommand.InvokeCanExecuteChanged();
        }
        private void OnGetNewPerson(object commandParameter)
        {
            CurrentPerson = GetPerson();
            _getnewPersonCommand.InvokeCanExecuteChanged();
        }
        private Mvvm_Person GetPerson()
        {
            Random rnd = new Random();
            int current = rnd.Next(1, 10);
            PeopleService ps = new PeopleService();
            List<Mvvm_Person> plist = ps.GetList();
            return plist[current - 1];
        }

        private bool CanUpdatePerson(object commandParameter)
        {
            // Can always update
            return true;
        }
        private bool CanGetNewPerson(object commandParameter)
        {
            // Once we happen upon final person, we are done getting new people
            //    return CurrentPerson.FirstName != "Final";
            return true;
        }

        private async void ValidateURL(string uRLName, [CallerMemberName] String propertyName = "")
        {
            await Task.Factory.StartNew(() =>
            {
                // Remove the existing errors for this property
                List<ValidationResult> existingErrors;
                asyncErrors.TryRemove(propertyName, out existingErrors);

                // Check for valid URL
                var results = new List<ValidationResult>();
                var vc = new ValidationContext(this) { MemberName = propertyName };
                if (!Validator.TryValidateProperty(this.URLName, vc, results) && results.Count > 0)
                {
                    asyncErrors.AddOrUpdate(propertyName, new List<ValidationResult>()
                    { new System.ComponentModel.DataAnnotations.ValidationResult(results[0].ErrorMessage)
                    },
                    (key, existingVal) =>
                    {
                        return new List<ValidationResult>()
                    { new ValidationResult(results[0].ErrorMessage)
                    };
                    });
                    NotifyErrorsChanged(propertyName);
                }
            });
        }
        /// <summary>
        /// This works to catch errors, but I've had issues trying to bind the errors caught here, with the view, so
        /// that they are visible to the user.
        /// </summary>
        protected override void Validate(string propertyName)
        {
            asyncErrors.Clear();
            errors.Clear();
            switch (propertyName)
            {
                case "CurrentPerson.FirstName":
                    if (String.IsNullOrEmpty(CurrentPerson.FirstName))
                        AddError("CurrentPerson.FirstName", "First Name is required", false);
                    break;
                case "CurrentPerson.LastName":
                    if (String.IsNullOrEmpty(CurrentPerson.LastName))
                        AddError("CurrentPerson.LastName", "Last Name is required", false);
                    break;
                case "CurrentPerson.Street":
                    if (String.IsNullOrEmpty(CurrentPerson.Street))
                        AddError("CurrentPerson.Street", "Street Address is required", false);
                    break;
                case "CurrentPerson":
                    if (String.IsNullOrEmpty(CurrentPerson.FirstName))
                        AddError("CurrentPerson.FirstName", "First Name is required", false);
                    if (String.IsNullOrEmpty(CurrentPerson.LastName))
                        AddError("CurrentPerson.LastName", "Last Name is required", false);
                    if (String.IsNullOrEmpty(CurrentPerson.Street))
                        AddError("CurrentPerson.Street", "Street Address is required", false);
                    break;
            }
        }
        protected bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }


}
