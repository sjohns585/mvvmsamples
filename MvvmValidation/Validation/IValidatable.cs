﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MvvmValidation.Validation
{
    public interface IValidatable : IDataErrorInfo
    {
        ObservableCollection<Tuple<string, ValidationError>> InvalidProperties
        { get; }
        bool IsValid { get; }
    }
}
