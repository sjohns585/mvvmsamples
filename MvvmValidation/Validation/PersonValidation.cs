﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvvmValidation.Validation
{
    public class PersonValidation : System.Windows.Controls.ValidationRule
    {
        public override System.Windows.Controls.ValidationResult Validate(object value, CultureInfo ultureInfo)
        {
            if (String.IsNullOrWhiteSpace(value as string))
            {
                return new System.Windows.Controls.ValidationResult(false, "Required!");
            }
            else
            {
                return new System.Windows.Controls.ValidationResult(true, null);
            }
        }
    }
}